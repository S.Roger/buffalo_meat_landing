<footer id="footer" class="footer-wrapper">
    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1892121363">
        <div class="img-inner dark">
            <img width="1920" height="112" src="images/co-xanh.jpg" class="attachment-original size-original lazyloaded"
                alt="trâu gác bếp" sizes="(max-width: 1920px) 100vw, 1920px"
                srcset="images/co-xanh.jpg 1920w, images/co-xanh-510x30.jpg 510w, images/co-xanh-300x18.jpg 300w, images/co-xanh-768x45.jpg 768w, images/co-xanh.jpg 1024w, images/co-xanh-1080x63.jpg 1080w"
                data-ll-status="loaded"><noscript><img width="1920" height="112" src="images/co-xanh.jpg"
                    class="attachment-original size-original" alt="trâu gác bếp"
                    srcset="images/co-xanh.jpg 1920w, images/co-xanh-510x30.jpg 510w, images/co-xanh-300x18.jpg 300w, images/co-xanh-768x45.jpg 768w, images/co-xanh.jpg 1024w, images/co-xanh-1080x63.jpg 1080w"
                    sizes="(max-width: 1920px) 100vw, 1920px" /></noscript>
        </div>
        <style>
            #image_1892121363 {
                width: 100%;
            }

        </style>
    </div>
    <div class="message-box relative dark" style="padding-top:30px;padding-bottom:30px;">
        <div class="message-box-bg-image bg-fill fill rocket-lazyload lazyloaded"
            style="background-image: url(&quot;images/bg-1024x82.png&quot;);" data-ll-status="loaded"></div>
        <div class="message-box-bg-overlay bg-fill fill" style="background-color:rgba(0, 0, 0, 0.18);"></div>
        <div class="container relative">
            <div class="inner last-reset">
                <div class="row align-middle align-center" id="row-1752775741">
                    <div id="col-1052594512" class="col hide-for-small medium-10 small-12 large-10">
                        <div class="col-inner">
                            <h3>Thịt trâu gác bếp Sơn La chính hiệu giá rẻ!</h3>
                            <p>Giao hàng miễn phí tại Hà Nội, vận chuyển toàn quốc</p>

                        </div>
                    </div>
                    <div id="col-1704004644" class="col show-for-small medium-10 small-12 large-10">
                        <div class="col-inner">
                            <h2><span style="font-size: 115%;">Thịt trâu gác bếp Sơn La</span></h2>
                            <p>Giao hàng miễn phí tại Hà Nội, vận chuyển toàn quốc</p>
                        </div>
                        <style>
                            #col-1704004644>.col-inner {
                                margin: 0px 0px 20px 0px;
                            }

                        </style>
                    </div>
                    <div id="col-558116328" class="col medium-2 small-12 large-2">
                        <div class="col-inner text-center">
                            <a href="#dat_mua" target="_self" class="button white is-outline expand"
                                style="border-radius:99px;">
                                <span>Đặt mua</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section" id="section_42833250">
        <div class="bg section-bg fill bg-fill  bg-loaded">
        </div>
        <div class="section-content relative">
            <div class="row row-divided" id="row-803618546">
                <div id="col-1057743818" class="col medium-6 small-12 large-6">
                    <div class="col-inner">
                        <p><strong>Trâu gác bếp</strong> là đặc sản của đồng bào các dân tộc vùng núi Tây Bắc. Nguyên
                            liệu từ thịt trâu tươi, ướp với gia vị gia truyền đặc biệt rồi sấy khô trên khói bếp. Đảm
                            bảo vệ sinh an toàn thực phẩm, giá trị dinh dưỡng cao.</p>
                        <p data-show="hide-for-small"><em>Cam kết thịt trâu sạch, giao hàng 24/7 toàn quốc.</em></p>
                        <hr>
                        <p data-show="hide-for-small"><span style="font-size: 95%;"><a
                                    href="https://xn--trugcbp-kwag9657e.com/#gioi_thieu">Giới thiệu</a>&nbsp; |&nbsp; <a
                                    href="/huong-dan/">Hướng dẫn</a>&nbsp; |&nbsp; <a
                                    href="https://xn--trugcbp-kwag9657e.com/quy-dinh-su-dung/">Quy định</a>&nbsp;
                                |&nbsp; <a href="https://xn--trugcbp-kwag9657e.com/bao-mat-thong-tin/">Chính
                                    sách</a></span></p>
                        <p style="text-align: center;" data-show="show-for-small"><span style="font-size: 90%;"><a
                                    href="https://xn--trugcbp-kwag9657e.com/#gioi_thieu">Giới thiệu</a>&nbsp; |&nbsp; <a
                                    href="/huong-dan/">Hướng dẫn</a>&nbsp; |&nbsp; <a
                                    href="https://xn--trugcbp-kwag9657e.com/quy-dinh-su-dung/">Quy định</a>&nbsp;
                                |&nbsp; <a href="https://xn--trugcbp-kwag9657e.com/bao-mat-thong-tin/">Chính
                                    sách</a></span></p>
                        <div id="gap-715386460" class="gap-element clearfix hide-for-medium"
                            style="display:block; height:auto;">

                            <style>
                                #gap-715386460 {
                                    padding-top: 20px;
                                }

                            </style>
                        </div>



                    </div>

                    <style>
                        #col-1057743818>.col-inner {
                            margin: 0px 0px -60px 0px;
                        }

                    </style>
                </div>



                <div id="col-1876178442" class="col medium-6 small-12 large-6">
                    <div class="col-inner">



                        <div class="row" id="row-332320353">


                            <div id="col-537239842" class="col medium-8 small-12 large-8">
                                <div class="col-inner">



                                    <p>Điện thoại: <a href="tel:0971591259"><strong>0971 59 12 59</strong></a><br>Email:
                                        traugacbep@gmail.com<br>Địa chỉ: Hoàng Mai, TP Hà Nội</p>

                                </div>

                                <style>
                                    #col-537239842>.col-inner {
                                        margin: 0px 0px -30px 0px;
                                    }

                                </style>
                            </div>



                            <div id="col-423882710" class="col hide-for-medium medium-4 small-12 large-4">
                                <div class="col-inner">




                                    <div class="icon-box featured-box icon-box-left text-left"
                                        style="margin:0px 0px 10px 0px;">
                                        <div class="icon-box-img" style="width: 26px">
                                            <div class="icon">
                                                <div class="icon-inner">
                                                    <img width="300" height="300" src="images/zalo.png"
                                                        class="attachment-medium size-medium lazyloaded" alt="zalo"
                                                        sizes="(max-width: 300px) 100vw, 300px"
                                                        srcset="images/zalo.png 300w, images/zalo-150x150.png 150w, images/zalo-100x100.png 100w"
                                                        data-ll-status="loaded"><noscript><img width="300" height="300"
                                                            src="images/zalo.png" class="attachment-medium size-medium"
                                                            alt="zalo"
                                                            srcset="images/zalo.png 300w, images/zalo-150x150.png 150w, images/zalo-100x100.png 100w"
                                                            sizes="(max-width: 300px) 100vw, 300px" /></noscript>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="icon-box-text last-reset">


                                            <p>097 159 1259</p>

                                        </div>
                                    </div>



                                    <div class="icon-box featured-box icon-box-left text-left"
                                        style="margin:0px 0px 10px 0px;">
                                        <div class="icon-box-img" style="width: 28px">
                                            <div class="icon">
                                                <div class="icon-inner">
                                                    <img width="100" height="100" src="images/shopee.png"
                                                        class="attachment-medium size-medium lazyloaded" alt="shopee"
                                                        data-ll-status="loaded"><noscript><img width="100" height="100"
                                                            src="images/shopee.png"
                                                            class="attachment-medium size-medium"
                                                            alt="shopee" /></noscript>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="icon-box-text last-reset">


                                            <p><a href="https://shopee.vn/linhgao2017" target="_blank"
                                                    rel="noopener noreferrer">Shopee Store</a></p>

                                        </div>
                                    </div>



                                    <div class="icon-box featured-box icon-box-left text-left">
                                        <div class="icon-box-img" style="width: 26px">
                                            <div class="icon">
                                                <div class="icon-inner">
                                                    <img width="256" height="256" src="images/sendo.png"
                                                        class="attachment-medium size-medium lazyloaded" alt="sendo"
                                                        sizes="(max-width: 256px) 100vw, 256px"
                                                        srcset="images/sendo.png 256w, images/sendo-150x150.png 150w"
                                                        data-ll-status="loaded"><noscript><img width="256" height="256"
                                                            src="images/sendo.png" class="attachment-medium size-medium"
                                                            alt="sendo"
                                                            srcset="images/sendo.png 256w, images/sendo-150x150.png 150w"
                                                            sizes="(max-width: 256px) 100vw, 256px" /></noscript>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="icon-box-text last-reset">


                                            <p><a href="https://www.sendo.vn/shop/trau-gac-bep-sl" target="_blank"
                                                    rel="noopener noreferrer">Sendo Store</a></p>

                                        </div>
                                    </div>



                                </div>

                                <style>
                                    #col-423882710>.col-inner {
                                        margin: 0px 0px -5px 0px;
                                    }

                                </style>
                            </div>



                        </div>
                        <div class="row" id="row-2067577592">


                            <div id="col-2090923944" class="col hide-for-medium medium-4 small-4 large-3">
                                <div class="col-inner text-center">



                                    <h5>Mạng xã hội</h5>

                                </div>

                                <style>
                                    #col-2090923944>.col-inner {
                                        margin: 5px 0px -30px 0px;
                                    }

                                </style>
                            </div>



                            <div id="col-907715831" class="col show-for-small medium-6 small-12 large-5">
                                <div class="col-inner text-center">



                                    <div class="social-icons follow-icons" style="font-size:80%"><a
                                            href="https://www.facebook.com/traugacbepsl/" target="_blank"
                                            data-label="Facebook" rel="noopener noreferrer nofollow"
                                            class="icon primary button circle facebook tooltip tooltipstered"><i
                                                class="icon-facebook"></i></a><a href="#" target="_blank"
                                            data-label="Twitter" rel="noopener noreferrer nofollow"
                                            class="icon primary button circle  twitter tooltip tooltipstered"><i
                                                class="icon-twitter"></i></a><a href="#" target="_blank"
                                            rel="noopener noreferrer nofollow" data-label="Pinterest"
                                            class="icon primary button circle  pinterest tooltip tooltipstered"><i
                                                class="icon-pinterest"></i></a><a href="#" target="_blank"
                                            rel="noopener noreferrer nofollow" data-label="LinkedIn"
                                            class="icon primary button circle  linkedin tooltip tooltipstered"><i
                                                class="icon-linkedin"></i></a><a
                                            href="https://www.youtube.com/channel/UCiIQ57l8ClrEeoijRLdPA6Q"
                                            target="_blank" rel="noopener noreferrer nofollow" data-label="YouTube"
                                            class="icon primary button circle  youtube tooltip tooltipstered"><i
                                                class="icon-youtube"></i></a></div>


                                </div>

                                <style>
                                    #col-907715831>.col-inner {
                                        margin: 0px 0px -40px 0px;
                                    }

                                </style>
                            </div>



                            <div id="col-859975422" class="col hide-for-small medium-6 small-12 large-5">
                                <div class="col-inner text-left">



                                    <div class="social-icons follow-icons" style="font-size:80%"><a
                                            href="https://www.facebook.com/traugacbepsl/" target="_blank"
                                            data-label="Facebook" rel="noopener noreferrer nofollow"
                                            class="icon primary button circle facebook tooltip tooltipstered"><i
                                                class="icon-facebook"></i></a><a
                                            href="https://www.instagram.com/traugacbepsl/" target="_blank"
                                            rel="noopener noreferrer nofollow" data-label="Instagram"
                                            class="icon primary button circle  instagram tooltip tooltipstered"><i
                                                class="icon-instagram"></i></a><a
                                            href="https://twitter.com/traugacbepsl" target="_blank" data-label="Twitter"
                                            rel="noopener noreferrer nofollow"
                                            class="icon primary button circle  twitter tooltip tooltipstered"><i
                                                class="icon-twitter"></i></a><a
                                            href="https://www.pinterest.com/traugacbepevi/" target="_blank"
                                            rel="noopener noreferrer nofollow" data-label="Pinterest"
                                            class="icon primary button circle  pinterest tooltip tooltipstered"><i
                                                class="icon-pinterest"></i></a><a
                                            href="https://www.linkedin.com/company/trau-gac-bep/" target="_blank"
                                            rel="noopener noreferrer nofollow" data-label="LinkedIn"
                                            class="icon primary button circle  linkedin tooltip tooltipstered"><i
                                                class="icon-linkedin"></i></a><a
                                            href="https://www.youtube.com/channel/UCiIQ57l8ClrEeoijRLdPA6Q"
                                            target="_blank" rel="noopener noreferrer nofollow" data-label="YouTube"
                                            class="icon primary button circle  youtube tooltip tooltipstered"><i
                                                class="icon-youtube"></i></a></div>


                                </div>

                                <style>
                                    #col-859975422>.col-inner {
                                        margin: 0px 0px -30px 0;
                                    }

                                </style>
                            </div>



                            <div id="col-175164359" class="col hide-for-medium medium-4 small-12 large-4">
                                <div class="col-inner text-right">



                                    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1238175910">
                                        <div class="img-inner image-color dark" style="margin:-6px 0px 0px 0px;">
                                            <img width="300" height="100" src="images/dmca_logo.png"
                                                class="attachment-original size-original lazyloaded" alt="dmca ogo"
                                                data-ll-status="loaded"><noscript><img width="300" height="100"
                                                    src="images/dmca_logo.png" class="attachment-original size-original"
                                                    alt="dmca ogo" /></noscript>
                                        </div>

                                        <style>
                                            #image_1238175910 {
                                                width: 70%;
                                            }

                                        </style>
                                    </div>



                                </div>

                                <style>
                                    #col-175164359>.col-inner {
                                        margin: 0px 0px -30px 0px;
                                    }

                                </style>
                            </div>



                        </div>

                    </div>

                    <style>
                        #col-1876178442>.col-inner {
                            margin: 0px 0px -30px 0px;
                        }

                    </style>
                </div>




                <style>
                    #row-803618546>.col>.col-inner {
                        padding: 30px 0px 20px 0px;
                    }

                </style>
            </div>

        </div>


        <style>
            #section_42833250 {
                padding-top: 30px;
                padding-bottom: 30px;
            }

        </style>
    </section>

    <div class="absolute-footer dark medium-text-center text-center">
        <div class="container clearfix">


            <div class="footer-primary pull-left">
                <div class="copyright-footer">
                    Copyright 2020 © by <a href="https://xn--trugcbp-kwag9657e.com/">Thịt trâu gác bếp</a> </div>
            </div>
        </div>
    </div>
    <a href="#top"
        class="back-to-top button icon invert plain fixed bottom z-1 is-outline hide-for-medium circle active"
        id="top-link"><i class="icon-angle-up"></i></a>

</footer>
