<header id="header" class="header transparent has-transparent">
    <div class="header-wrapper">
        <div id="masthead" class="header-main hide-for-sticky">
            <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">
                <!-- Logo -->
                <div id="logo" class="flex-col logo">
                    <!-- Header logo -->
                    <a href="https://xn--trugcbp-kwag9657e.com/"
                        title="Trâu gác bếp - Thịt trâu gác bếp Tây Bắc chính hiệu giá rẻ ngon nhất" rel="home">
                        <img width="200" height="100" src="images/trau-logo-1.png"
                            class="header_logo header-logo lazyloaded" alt="Trâu gác bếp"
                            data-ll-status="loaded"><noscript><img width="200" height="100" src="images/trau-logo-1.png"
                                class="header_logo header-logo" alt="Trâu gác bếp" /></noscript><img width="200"
                            height="100"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20200%20100'%3E%3C/svg%3E"
                            class="header-logo-dark" alt="Trâu gác bếp"
                            data-lazy-src="images/trau-logo-1.png"><noscript><img width="200" height="100"
                                src="images/trau-logo-1.png" class="header-logo-dark"
                                alt="Trâu gác bếp" /></noscript></a>
                </div>
                <!-- Mobile Left Elements -->
                <div class="flex-col show-for-medium flex-left">
                    <ul class="mobile-nav nav nav-left ">
                        <li class="header-search header-search-lightbox has-icon">
                            <a href="#search-lightbox" aria-label="Search" data-open="#search-lightbox"
                                data-focus="input.search-field" class="is-small">
                                <i class="icon-search" style="font-size:16px;"></i></a>

                            <div id="search-lightbox" class="mfp-hide dark text-center">
                                <div class="searchform-wrapper ux-search-box relative is-large">
                                    <form method="get" class="searchform" action="https://xn--trugcbp-kwag9657e.com/"
                                        role="search">
                                        <div class="flex-row relative">
                                            <div class="flex-col flex-grow">
                                                <input type="search" class="search-field mb-0" name="s" value="" id="s"
                                                    placeholder="Search…" autocomplete="off">
                                            </div>
                                            <div class="flex-col">
                                                <button type="submit"
                                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                                    aria-label="Submit">
                                                    <i class="icon-search"></i> </button>
                                            </div>
                                        </div>
                                        <div class="live-search-results text-left z-top">
                                            <div class="autocomplete-suggestions"
                                                style="position: absolute; display: none; max-height: 300px; z-index: 9999;">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- Left Elements -->
                <div class="flex-col hide-for-medium flex-left
            flex-grow">
                    <ul
                        class="header-nav header-nav-main nav nav-left  nav-size-medium nav-spacing-large nav-uppercase">
                    </ul>
                </div>
                <!-- Right Elements -->
                <div class="flex-col hide-for-medium flex-right">
                    <ul
                        class="header-nav header-nav-main nav nav-right  nav-size-medium nav-spacing-large nav-uppercase">
                        <li id="menu-item-28271"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28271 introduce"><a
                                href="#gioi_thieu" class="nav-top-link">GIỚI THIỆU</a></li>
                        <li id="menu-item-28272"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28272"><a
                                href="#trau_gac_bep" class="nav-top-link">TRÂU GÁC BẾP</a></li>
                        <li id="menu-item-28585"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28585"><a
                                href="#khach_hang" class="nav-top-link">KHÁCH HÀNG</a></li>
                        <li id="menu-item-28275"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28275"><a
                                href="#lien_he" class="nav-top-link">LIÊN HỆ</a></li>
                        <li id="menu-item-28615"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28615"><a
                                href="#faq" class="nav-top-link">FAQ</a></li>
                        <li class="html custom html_topbar_right">
                            <div id="dat_mua" class="lightbox-by-id lightbox-content lightbox-white mfp-hide"
                                style="max-width:600px ;padding:5px">
                                <section class="section" id="section_137708845">
                                    <div class="bg section-bg fill bg-fill   bg-loaded">
                                        <div class="section-bg-overlay absolute fill"></div>
                                    </div>
                                    <div class="section-content relative">
                                        <div class="row align-center" id="row-323009837">
                                            <div id="col-2027755705" class="col medium-8 small-12 large-8">
                                                <div class="col-inner text-center dark">
                                                    <h3>ĐẶT MUA TRÂU GÁC BẾP</h3>
                                                    <div id="gap-675119138" class="gap-element clearfix"
                                                        style="display:block; height:auto;">

                                                        <style>
                                                            #gap-675119138 {
                                                                padding-top: 20px;
                                                            }

                                                        </style>
                                                    </div>


                                                    <div role="form" class="wpcf7" id="wpcf7-f29390-o1" lang="vi"
                                                        dir="ltr">
                                                        <div class="screen-reader-response">
                                                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                            <ul></ul>
                                                        </div>
                                                        <form action="/#wpcf7-f29390-o1" method="post"
                                                            class="wpcf7-form init" novalidate="novalidate"
                                                            data-status="init">
                                                            <div style="display: none;">
                                                                <input type="hidden" name="_wpcf7" value="29390">
                                                                <input type="hidden" name="_wpcf7_version" value="5.3">
                                                                <input type="hidden" name="_wpcf7_locale" value="vi">
                                                                <input type="hidden" name="_wpcf7_unit_tag"
                                                                    value="wpcf7-f29390-o1">
                                                                <input type="hidden" name="_wpcf7_container_post"
                                                                    value="0">
                                                                <input type="hidden" name="_wpcf7_posted_data_hash"
                                                                    value="">
                                                                <input type="hidden" name="_wpcf7_recaptcha_response"
                                                                    value="03AGdBq25NvUSQhq5nrU7wZkOw1PdQqz8nwANoQk3uHKl0O6fLRQuJON8sPWtIj_Itl49plzL3evdI1S1PmoRErxb4nbEywpOsbZsStdh-aSd9htTQo3QmgzKkjdyySmWtIO94qhLS3LebGbExGr07H8VLmpGTgLDedKsAxop1VsUiggSsJeuQso80_hmZdWd3AUkhFl-BWUUzKpD2RTVIU7Ceh89XvZ-UbH431imZadyNJL8Qk-2-aKWB2z9FOpRSStcZdXYm5IVpLNTGUWqZyNN0C5ouQa31kSBzLiLqY4FKPm7PpHc84YZo84pTP2K9FY1LOem37G8byjyqJsn3Vjlvq9b6BURVbp8WeM1HbL_mJActxQe-ecqPfaXEcUBASRQVfb5nzIoCjSXbopSFhGpzlv80-Ut7umBf2Grl6Ti-Z4KVd6brnEbTkD6aU3CbSasLehXIdQo5Yu8OOfTPNw50ZSPh2d-zWLaioOWgKvJUymtvQ8_Jq2y1SFq5OnAyDb2bHIzBUA6v7Rv6IY1tUn5lKGp-GjDvcg">
                                                            </div>
                                                            <p><span class="wpcf7-form-control-wrap dienthoai"><input
                                                                        type="tel" name="dienthoai" value="" size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                        aria-required="true" aria-invalid="false"
                                                                        placeholder="Điện thoại*"></span><br>
                                                                <span
                                                                    class="wpcf7-form-control-wrap emailmuatrau"><input
                                                                        type="email" name="emailmuatrau" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                        aria-required="true" aria-invalid="false"
                                                                        placeholder="Email*"></span><br>
                                                                <span
                                                                    class="wpcf7-form-control-wrap diachigiaohang"><input
                                                                        type="text" name="diachigiaohang" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        id="6" aria-required="true" aria-invalid="false"
                                                                        placeholder="Địa chỉ giao hàng*"></span>
                                                            </p>
                                                            <p><b>Chọn trọng lượng</b></p>
                                                            <hr>
                                                            <span class="wpcf7-form-control-wrap soluongsp"><select
                                                                    name="soluongsp"
                                                                    class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required"
                                                                    aria-required="true" aria-invalid="false">
                                                                    <option value="0,5kg giá 415.000đ">0,5kg giá
                                                                        415.000đ</option>
                                                                    <option value="1kg giá 820.000đ">1kg giá 820.000đ
                                                                    </option>
                                                                    <option value="2kg giá 1.620.000đ">2kg giá
                                                                        1.620.000đ</option>
                                                                    <option value="Đặt sỉ số lượng lớn">Đặt sỉ số lượng
                                                                        lớn</option>
                                                                </select></span><br>
                                                            <span class="wpcf7-form-control-wrap ghichu"><input
                                                                    type="text" name="ghichu" value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text" id="6"
                                                                    aria-invalid="false"
                                                                    placeholder="Ghi chú thêm..."></span>
                                                            <p></p>
                                                            <p><input type="submit" value="Đặt mua"
                                                                    class="wpcf7-form-control wpcf7-submit button"><span
                                                                    class="ajax-loader"></span></p>
                                                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                                                        </form>
                                                    </div>


                                                </div>

                                                <style>
                                                    #col-2027755705>.col-inner {
                                                        padding: 0px 8px 0px 8px;
                                                        margin: 20px 6px -60px 0px;
                                                    }

                                                </style>
                                            </div>



                                        </div>

                                    </div>


                                    <style>
                                        #section_137708845 {
                                            padding-top: 30px;
                                            padding-bottom: 30px;
                                        }

                                        #section_137708845 .section-bg-overlay {
                                            background-color: rgba(0, 0, 0, 0.38);
                                        }

                                        #section_137708845 .section-bg.bg-loaded {
                                            background-image: url(images/bg.png);
                                        }

                                        #section_137708845 .section-bg {
                                            background-position: 100% 47%;
                                        }

                                    </style>
                                </section>
                            </div>

                        </li>
                        <li class="html header-button-1">
                            <div class="header-button">
                                <a href="tel:0971591259" class="button primary" style="border-radius:99px;">
                                    <span>Hotline: 0971 59 12 59</span>
                                </a>
                            </div>
                        </li>


                    </ul>
                </div>

                <!-- Mobile Right Elements -->
                <div class="flex-col show-for-medium flex-right">
                    <ul class="mobile-nav nav nav-right ">
                        <li class="nav-icon has-icon">
                            <a href="#" data-open="#main-menu" data-pos="right" data-bg="main-menu-overlay"
                                data-color="" class="is-small" aria-label="Menu" aria-controls="main-menu"
                                aria-expanded="false">

                                <i class="icon-menu"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="container">
                <div class="top-divider full-width"></div>
            </div>
        </div>
        <div class="header-bg-container fill">
            <div class="header-bg-image fill"></div>
            <div class="header-bg-color fill"></div>
        </div>
    </div>
</header>
