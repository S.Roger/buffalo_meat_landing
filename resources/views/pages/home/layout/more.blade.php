<section class="section hide-for-small" id="section_1050338884">
    <div class="bg section-bg fill bg-fill  bg-loaded">





    </div>

    <div class="section-content relative">


        <div class="row" id="row-1914779982">


            <div id="col-1793171426" class="col medium-4 small-12 large-4">
                <div class="col-inner">



                    <h3>1/ Trâu gác bếp Sơn La</h3>
                    <hr>
                    <p>Đặc sản trâu khô Tây Bắc – <strong>Thịt trâu gác bếp Sơn La</strong> vị ngon số 1 trong các loại
                        trâu khô. Gia vị đặc trưng của người Thái vô cùng đặc trưng.</p>
                    <div class="img has-hover hide-for-medium x md-x lg-x y md-y lg-y" id="image_1268252171">
                        <div class="img-inner image-cover dark" style="padding-top:340px;">
                            <img width="460" height="460" src="images/say-thit-trau-gac-bep.jpg"
                                data-src="images/say-thit-trau-gac-bep.jpg"
                                class="attachment-large size-large lazy-load-active" alt="Trâu gác bếp Sơn La"
                                srcset="images/say-thit-trau-gac-bep.jpg 460w, images/say-thit-trau-gac-bep-150x150.jpg 150w, images/say-thit-trau-gac-bep-300x300.jpg 300w, images/say-thit-trau-gac-bep-100x100.jpg 100w"
                                data-srcset="images/say-thit-trau-gac-bep.jpg 460w, images/say-thit-trau-gac-bep-150x150.jpg 150w, images/say-thit-trau-gac-bep-300x300.jpg 300w, images/say-thit-trau-gac-bep-100x100.jpg 100w"
                                sizes="(max-width: 460px) 100vw, 460px">
                            <div class="caption">Gác bếp thịt trâu Sơn La</div>
                        </div>

                        <style>
                            #image_1268252171 {
                                width: 100%;
                            }

                        </style>
                    </div>


                    <div class="img has-hover show-for-small x md-x lg-x y md-y lg-y" id="image_444491827">
                        <div class="img-inner image-cover dark" style="padding-top:340px;">
                            <img width="460" height="460"
                                src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20460%20460%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                data-src="images/say-thit-trau-gac-bep.jpg"
                                class="lazy-load attachment-large size-large" alt="Trâu gác bếp Sơn La" srcset=""
                                data-srcset="images/say-thit-trau-gac-bep.jpg 460w, images/say-thit-trau-gac-bep-150x150.jpg 150w, images/say-thit-trau-gac-bep-300x300.jpg 300w, images/say-thit-trau-gac-bep-100x100.jpg 100w"
                                sizes="(max-width: 460px) 100vw, 460px">
                            <div class="caption">Gác bếp thịt trâu Sơn La</div>
                        </div>

                        <style>
                            #image_444491827 {
                                width: 100%;
                            }

                        </style>
                    </div>



                </div>

                <style>
                    #col-1793171426>.col-inner {
                        margin: 0px 0px -30px 0px;
                    }

                </style>
            </div>



            <div id="col-1785527726" class="col medium-4 small-12 large-4">
                <div class="col-inner">



                    <h3>2/ Trâu gác bếp Điện Biên</h3>
                    <hr>
                    <p>Đặc sản của đồng bào dân tộc Tày, Thái có <strong>Trâu gác bếp Điện Biên</strong> được ướp gia vị
                        tỏi, ớt tươi, gừng, muối ăn, hạt sẻn riêng của vùng. Cực ngon!</p>
                    <div class="img has-hover hide-for-medium x md-x lg-x y md-y lg-y" id="image_100554681">
                        <div class="img-inner image-cover dark" style="padding-top:340px;">
                            <img width="460" height="460" src="images/thai-thit-trau-gac-bep.jpg"
                                data-src="images/thai-thit-trau-gac-bep.jpg"
                                class="attachment-large size-large lazy-load-active" alt="Trâu gác bếp Điện Biên"
                                srcset="images/thai-thit-trau-gac-bep.jpg 460w, images/thai-thit-trau-gac-bep-150x150.jpg 150w, images/thai-thit-trau-gac-bep-300x300.jpg 300w, images/thai-thit-trau-gac-bep-100x100.jpg 100w"
                                data-srcset="images/thai-thit-trau-gac-bep.jpg 460w, images/thai-thit-trau-gac-bep-150x150.jpg 150w, images/thai-thit-trau-gac-bep-300x300.jpg 300w, images/thai-thit-trau-gac-bep-100x100.jpg 100w"
                                sizes="(max-width: 460px) 100vw, 460px">
                            <div class="caption">Thái thịt trâu gác bếp Điện Biên</div>
                        </div>

                        <style>
                            #image_100554681 {
                                width: 100%;
                            }

                        </style>
                    </div>



                </div>

                <style>
                    #col-1785527726>.col-inner {
                        margin: 0px 0px -30px 0px;
                    }

                </style>
            </div>



            <div id="col-2063899494" class="col medium-4 small-12 large-4">
                <div class="col-inner">



                    <h3>3/ Trâu gác bếp Sapa</h3>
                    <hr>
                    <p>Núi rừng Lào Cai mang đến món ngon nức lòng thực khách: <strong>Trâu gác bếp Sapa</strong> gia
                        truyền. Hòa quyện vị trâu khô và gia vị đặc trưng tại Sapa.</p>
                    <div class="img has-hover hide-for-medium x md-x lg-x y md-y lg-y" id="image_972484014">
                        <div class="img-inner image-cover dark" style="padding-top:340px;">
                            <img width="460" height="460" src="images/uop-thit-trau-gac-bep.jpg"
                                data-src="images/uop-thit-trau-gac-bep.jpg"
                                class="attachment-large size-large lazy-load-active" alt="Trâu gác bếp Sapa"
                                srcset="images/uop-thit-trau-gac-bep.jpg 460w, images/uop-thit-trau-gac-bep-150x150.jpg 150w, images/uop-thit-trau-gac-bep-300x300.jpg 300w, images/uop-thit-trau-gac-bep-100x100.jpg 100w"
                                data-srcset="images/uop-thit-trau-gac-bep.jpg 460w, images/uop-thit-trau-gac-bep-150x150.jpg 150w, images/uop-thit-trau-gac-bep-300x300.jpg 300w, images/uop-thit-trau-gac-bep-100x100.jpg 100w"
                                sizes="(max-width: 460px) 100vw, 460px">
                            <div class="caption">Ướp thịt trâu gác bếp Sapa</div>
                        </div>

                        <style>
                            #image_972484014 {
                                width: 100%;
                            }

                        </style>
                    </div>


                    <div id="gap-1575131386" class="gap-element clearfix hide-for-small"
                        style="display:block; height:auto;">

                        <style>
                            #gap-1575131386 {
                                padding-top: 10px;
                            }

                        </style>
                    </div>



                </div>

                <style>
                    #col-2063899494>.col-inner {
                        margin: 0px 0px -30px 0px;
                    }

                </style>
            </div>




            <style>
                #row-1914779982>.col>.col-inner {
                    padding: 20px 0px 0px 0px;
                }

            </style>
        </div>

    </div>


    <style>
        #section_1050338884 {
            padding-top: 30px;
            padding-bottom: 30px;
        }

    </style>
</section>
