<div class="slider-wrapper relative show-for-small" id="slider-1831832442">
    <div class="slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal is-draggable flickity-enabled slider-lazy-load-active"
        data-flickity-options="{
            &quot;cellAlign&quot;: &quot;center&quot;,
            &quot;imagesLoaded&quot;: true,
            &quot;lazyLoad&quot;: 1,
            &quot;freeScroll&quot;: false,
            &quot;wrapAround&quot;: true,
            &quot;autoPlay&quot;: 6000,
            &quot;pauseAutoPlayOnHover&quot; : true,
            &quot;prevNextButtons&quot;: true,
            &quot;contain&quot; : true,
            &quot;adaptiveHeight&quot; : true,
            &quot;dragThreshold&quot; : 10,
            &quot;percentPosition&quot;: true,
            &quot;pageDots&quot;: true,
            &quot;rightToLeft&quot;: false,
            &quot;draggable&quot;: true,
            &quot;selectedAttraction&quot;: 0.1,
            &quot;parallax&quot; : 0,
            &quot;friction&quot;: 0.6        }" tabindex="0">
        <div class="flickity-viewport" style="height: 0px; touch-action: pan-y;">
            <div class="flickity-slider" style="left: 0px; transform: translateX(-300%);">
                <div class="banner has-hover" id="banner-280967003" aria-selected="false"
                    style="position: absolute; left: 400%;">
                    <div class="banner-inner fill">
                        <div class="banner-bg fill">
                            <div class="bg fill bg-fill  bg-loaded"></div>

                        </div>
                        <div class="banner-layers container">
                            <div class="fill banner-link"></div>

                            <div id="text-box-982948886"
                                class="text-box banner-layer x50 md-x50 lg-x50 y80 md-y50 lg-y50 res-text">
                                <div class="text dark">

                                    <div class="text-inner text-center">


                                        <h1 class="uppercase"><b>Trâu gác bếp</b></h1>
                                        <h3>Đặc sản trứ danh Tây Bắc</h3>

                                    </div>
                                </div>

                                <style>
                                    #text-box-982948886 {
                                        width: 92%;
                                    }

                                    #text-box-982948886 .text {
                                        font-size: 100%;
                                    }

                                    @media (min-width: 550px) {
                                        #text-box-982948886 {
                                            width: 60%;
                                        }
                                    }

                                </style>
                            </div>


                        </div>
                    </div>


                    <style>
                        #banner-280967003 {
                            padding-top: 400px;
                        }

                        #banner-280967003 .bg.bg-loaded {
                            background-image: url(images/thit-trau-gac-bep-son-la.jpg);
                        }

                        @media (min-width: 550px) {
                            #banner-280967003 {
                                padding-top: 500px;
                            }
                        }

                    </style>
                </div>
                <div class="banner has-hover" id="banner-1942207539" aria-selected="false"
                    style="position: absolute; left: 100%;">
                    <div class="banner-inner fill">
                        <div class="banner-bg fill">
                            <div class="bg fill bg-fill  bg-loaded"></div>

                        </div>
                        <div class="banner-layers container">
                            <div class="fill banner-link"></div>

                            <div id="text-box-1650368936"
                                class="text-box banner-layer x50 md-x50 lg-x50 y80 md-y50 lg-y50 res-text">
                                <div class="text dark">

                                    <div class="text-inner text-center">


                                        <h2 class="uppercase"><b>Trâu khô tây bắc</b></h2>

                                    </div>
                                </div>

                                <style>
                                    #text-box-1650368936 {
                                        width: 92%;
                                    }

                                    #text-box-1650368936 .text {
                                        font-size: 100%;
                                    }

                                    @media (min-width: 550px) {
                                        #text-box-1650368936 {
                                            width: 60%;
                                        }
                                    }

                                </style>
                            </div>
                        </div>
                    </div>
                    <style>
                        #banner-1942207539 {
                            padding-top: 400px;
                        }

                        #banner-1942207539 .bg.bg-loaded {
                            background-image: url(images/thit-trau-gac-bep-pic-5.jpg);
                        }

                        @media (min-width: 550px) {
                            #banner-1942207539 {
                                padding-top: 500px;
                            }
                        }

                    </style>
                </div>
                <div class="banner has-hover" id="banner-1093914487" aria-selected="false"
                    style="position: absolute; left: 200%;">
                    <div class="banner-inner fill">
                        <div class="banner-bg fill">
                            <div class="bg fill bg-fill  bg-loaded"></div>

                        </div>
                        <div class="banner-layers container">
                            <div class="fill banner-link"></div>


                        </div>
                    </div>
                    <style>
                        #banner-1093914487 {
                            padding-top: 400px;
                        }

                        #banner-1093914487 .bg.bg-loaded {
                            background-image: url(images/thit-trau-gac-bep-pic-3.jpg);
                        }

                        @media (min-width: 550px) {
                            #banner-1093914487 {
                                padding-top: 500px;
                            }
                        }

                    </style>
                </div>
                <div class="banner has-hover is-selected" id="banner-1788096488" aria-selected="true"
                    style="position: absolute; left: 300%;">
                    <div class="banner-inner fill">
                        <div class="banner-bg fill">
                            <div class="bg fill bg-fill  bg-loaded"></div>

                        </div>
                        <div class="banner-layers container">
                            <div class="fill banner-link"></div>


                        </div>
                    </div>
                    <style>
                        #banner-1788096488 {
                            padding-top: 400px;
                        }

                        #banner-1788096488 .bg.bg-loaded {
                            background-image: url(images/trau-gac-bep-gia-re.jpg);
                        }

                        @media (min-width: 550px) {
                            #banner-1788096488 {
                                padding-top: 500px;
                            }
                        }

                    </style>
                </div>
            </div>
        </div>
        <button class="flickity-button flickity-prev-next-button previous" type="button" aria-label="Previous">
            <svg class="flickity-button-icon" viewBox="0 0 100 100">
                <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path>
            </svg>
        </button>
        <button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next">
            <svg class="flickity-button-icon" viewBox="0 0 100 100">
                <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"
                    transform="translate(100, 100) rotate(180) "></path>
            </svg>
        </button>
        <ol class="flickity-page-dots">
            <li class="dot" aria-label="Page dot 1"></li>
            <li class="dot" aria-label="Page dot 2"></li>
            <li class="dot" aria-label="Page dot 3"></li>
            <li class="dot is-selected" aria-label="Page dot 4" aria-current="step"></li>
        </ol>
    </div>

    <div class="loading-spin dark large centered" style="display: none;"></div>
</div>
