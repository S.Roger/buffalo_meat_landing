<div class="banner has-hover hide-for-small" id="banner-1777467494">
    <div class="banner-inner fill">
        <div class="banner-bg fill">
            <div class="bg fill bg-fill  bg-loaded"></div>
        </div>
        <div class="banner-layers container">
            <div class="fill banner-link"></div>

            <div id="text-box-36803890"
                 class="text-box banner-layer hide-for-small x0 md-x0 lg-x0 y50 md-y50 lg-y50 res-text">
                <div data-animate="fadeInRight" data-animated="true">
                    <div class="text ">

                        <div class="text-inner text-left">


                            <h1 class="uppercase">Trâu gác bếp</h1>
                            <h3><span style="font-size: 115%;">Đặc sản vùng cao Tây Bắc</span></h3>

                        </div>
                    </div>
                </div>
                <style>
                    #text-box-36803890 .text-inner {
                        padding: 0px 0px 0px 14px;
                    }

                    #text-box-36803890 {
                        width: 90%;
                    }

                    #text-box-36803890 .text {
                        font-size: 180%;
                    }

                    @media (min-width: 550px) {
                        #text-box-36803890 {
                            width: 50%;
                        }

                        #text-box-36803890 .text {
                            font-size: 90%;
                        }
                    }

                    @media (min-width: 850px) {
                        #text-box-36803890 {
                            width: 38%;
                        }
                    }
                </style>
            </div>
            <div id="text-box-166074327"
                 class="text-box banner-layer show-for-small x50 md-x0 lg-x0 y95 md-y50 lg-y50 res-text">
                <div data-animate="fadeInUp" data-animated="true">
                    <div class="text ">
                        <div class="text-inner text-left">
                            <h1 class="uppercase" style="text-align: center;">Trâu gác bếp</h1>
                            <h3 style="text-align: center;"><span style="font-size: 115%;">Đặc sản vùng cao Tây Bắc</span>
                            </h3>
                        </div>
                    </div>
                </div>
                <style>
                    #text-box-166074327 {
                        width: 100%;
                    }

                    #text-box-166074327 .text {
                        font-size: 100%;
                    }

                    @media (min-width: 550px) {
                        #text-box-166074327 {
                            width: 50%;
                        }

                        #text-box-166074327 .text {
                            font-size: 90%;
                        }
                    }

                    @media (min-width: 850px) {
                        #text-box-166074327 {
                            width: 38%;
                        }
                    }
                </style>
            </div>


        </div>
    </div>
    <style>
        #banner-1777467494 {
            padding-top: 420px;
        }

        #banner-1777467494 .bg.bg-loaded {
            background-image: url('images/bg.jpg');
        }

        #banner-1777467494 .bg {
            background-position: 63% 52%;
        }

        @media (min-width: 550px) {
            #banner-1777467494 {
                padding-top: 600px;
            }
        }
    </style>
</div>
