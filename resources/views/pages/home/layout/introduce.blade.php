<section class="section section-introduce" id="section_660260776">
    <div class="bg section-bg fill bg-fill  bg-loaded">


    </div>

    <div class="section-content relative">


        <div class="row" id="row-2126841453">


            <div id="col-178392488" class="col hide-for-small medium-6 small-12 large-6">
                <div class="col-inner box-shadow-5">


                    <div class="banner has-hover bg-blur" id="banner-558200624">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill  bg-loaded"></div>

                            </div>
                            <div class="banner-layers container">
                                <div class="fill banner-link"></div>

                                <div id="text-box-592455347"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
                                    <div class="text dark">

                                        <div class="text-inner text-center">


                                            <div class="video-button-wrapper" style="font-size:180%"><a
                                                    href="https://www.youtube.com/watch?v=CMysjs1Gwxo"
                                                    class="button open-video icon circle is-outline is-xlarge"><i
                                                        class="icon-play" style="font-size:1.5em;"></i></a>
                                            </div>


                                        </div>
                                    </div>

                                    <style>
                                        #text-box-592455347 {
                                            width: 60%;
                                        }

                                        #text-box-592455347 .text {
                                            font-size: 100%;
                                        }

                                    </style>
                                </div>


                            </div>
                        </div>


                        <style>
                            #banner-558200624 {
                                padding-top: 400px;
                            }

                            #banner-558200624 .bg.bg-loaded {
                                background-image: url(images/trau-gac-bep-dac-san-son-la.jpg);
                            }

                            #banner-558200624 .bg {
                                background-position: 53% 100%;
                            }

                            @media (min-width: 850px) {
                                #banner-558200624 {
                                    padding-top: 360px;
                                }
                            }

                        </style>
                    </div>


                </div>
            </div>


            <div id="col-1509149576" class="col medium-6 small-12 large-6">
                <div class="col-inner">


                    <div class="container section-title-container hide-for-small">
                        <h4 class="section-title section-title-normal"><b></b><span class="section-title-main"
                                style="font-size:90%;color:rgb(154, 154, 154);">Giới thiệu</span><b></b>
                        </h4>
                    </div>

                    <h2><span style="font-size: 120%;">Thịt trâu gác bếp</span></h2>
                    <p><strong><a data-mil="39" href="https://xn--trugcbp-kwag9657e.com/">Trâu gác
                                bếp</a></strong>&nbsp;là đặc sản&nbsp;từ núi rừng <strong>Tây Bắc</strong> – món
                        ngon làm nức lòng bao người thưởng thức. Hương vị thịt gác bếp lạ miệng ai mùi khói,
                        quyện cùng hạt Mắc khén, Dổi rừng, Hạt tiêu,… làm nên thương hiệu trâu khô trứ danh có
                        một không hai này. Món ăn đặc biệt lắm!</p>
                    <p><strong>Nguyên liệu</strong>: Thịt trâu tươi, gia vị đặc biệt.<br><strong>Chế
                            biến:</strong> Thịt trâu ướp gia vị, gác bếp để khô.<br><strong>Bảo quản</strong>:
                        Ngăn đá tủ lạnh, tủ đá bảo quản.<br><strong>Cách ăn:</strong> Làm nóng, xé phay ăn trực
                        tiếp.</p>

                </div>

                <style>
                    #col-1509149576>.col-inner {
                        margin: 0px 0px -30px 0px;
                    }

                </style>
            </div>


        </div>
        <div class="row" id="row-724530519">


            <div id="col-74698299" class="col medium-4 small-12 large-4">
                <div class="col-inner">


                    <div class="icon-box featured-box icon-box-left text-left">
                        <div class="icon-box-img" style="width: 42px">
                            <div class="icon">
                                <div class="icon-inner">
                                    <img width="64" height="64" src="images/check-mark.png"
                                        data-src="images/check-mark.png"
                                        class="attachment-medium size-medium lazy-load-active" alt="check icon">
                                </div>
                            </div>
                        </div>
                        <div class="icon-box-text last-reset">


                            <h3>Từ thịt trâu sạch</h3>
                            <p>Trâu thả đồi ăn cỏ của người dân tộc Thái Đen, nguyên liệu sạch.</p>

                        </div>
                    </div>


                </div>

                <style>
                    #col-74698299>.col-inner {
                        margin: 20px 0px -30px 0px;
                    }

                </style>
            </div>


            <div id="col-1408950324" class="col medium-4 small-12 large-4">
                <div class="col-inner">


                    <div class="icon-box featured-box icon-box-left text-left">
                        <div class="icon-box-img" style="width: 42px">
                            <div class="icon">
                                <div class="icon-inner">
                                    <img width="64" height="64" src="images/firewall.png" data-src="images/firewall.png"
                                        class="attachment-medium size-medium lazy-load-active" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="icon-box-text last-reset">


                            <h3>Không chất bảo quản</h3>
                            <p>Không sử dụng bất kỳ chất bảo quản nào. Chỉ sấy khô gác bếp.</p>

                        </div>
                    </div>


                </div>

                <style>
                    #col-1408950324>.col-inner {
                        margin: 20px 0px -30px 0px;
                    }

                </style>
            </div>


            <div id="col-663387851" class="col medium-4 small-12 large-4">
                <div class="col-inner">


                    <div class="icon-box featured-box icon-box-left text-left">
                        <div class="icon-box-img" style="width: 42px">
                            <div class="icon">
                                <div class="icon-inner">
                                    <img width="64" height="64" src="images/car.png" data-src="images/car.png"
                                        class="attachment-medium size-medium lazy-load-active" alt="car map">
                                </div>
                            </div>
                        </div>
                        <div class="icon-box-text last-reset">


                            <h3>Giao nhận toàn quốc</h3>
                            <p>Vận chuyển miễn phí, giao hàng toàn quốc mọi giá trị đơn hàng.</p>

                        </div>
                    </div>


                </div>

                <style>
                    #col-663387851>.col-inner {
                        margin: 20px 0px -30px 0px;
                    }

                </style>
            </div>


            <style>
                #row-724530519>.col>.col-inner {
                    padding: 20px 16px 20px 20px;
                    background-color: rgb(243, 244, 250);
                }

            </style>
        </div>
        <div id="gap-33741535" class="gap-element clearfix show-for-small" style="display:block; height:auto;">

            <style>
                #gap-33741535 {
                    padding-top: 20px;
                }

                @media (min-width: 550px) {
                    #gap-33741535 {
                        padding-top: 30px;
                    }
                }

            </style>
        </div>


        <div class="row show-for-small large-columns-4 medium-columns-3 small-columns-2 row-xsmall slider row-slider slider-nav-reveal slider-nav-light flickity-enabled slider-lazy-load-active"
            data-flickity-options="{&quot;imagesLoaded&quot;: true, &quot;groupCells&quot;: &quot;100%&quot;, &quot;dragThreshold&quot; : 5, &quot;cellAlign&quot;: &quot;left&quot;,&quot;wrapAround&quot;: true,&quot;prevNextButtons&quot;: true,&quot;percentPosition&quot;: true,&quot;pageDots&quot;: false, &quot;rightToLeft&quot;: false, &quot;autoPlay&quot; : 4000}"
            tabindex="0">


            <div class="flickity-viewport" style="height: 0px; touch-action: pan-y;">
                <div class="flickity-slider" style="left: 0px; transform: translateX(-200%);">
                    <div class="gallery-col col is-selected" aria-selected="true" style="position: absolute; left: 0%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-6.jpg"
                                title="trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560" src="images/thit-trau-gac-bep-pic-6.jpg"
                                            data-src="images/thit-trau-gac-bep-pic-6.jpg"
                                            class="attachment-original size-original lazy-load-active"
                                            alt="trâu gác bếp" ids="29718,29719,29720,29721,29722,29723,29724,29725"
                                            style="none" lightbox_image_size="original" type="slider"
                                            col_spacing="xsmall" columns__sm="2" slider_nav_color="light"
                                            auto_slide="4000" image_height="230px" image_size="original"
                                            visibility="show-for-small"
                                            srcset="images/thit-trau-gac-bep-pic-6.jpg 560w, images/thit-trau-gac-bep-pic-6-300x300.jpg 300w, images/thit-trau-gac-bep-pic-6-150x150.jpg 150w"
                                            data-srcset="images/thit-trau-gac-bep-pic-6.jpg 560w, images/thit-trau-gac-bep-pic-6-300x300.jpg 300w, images/thit-trau-gac-bep-pic-6-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 50%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-7.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560" src="images/thit-trau-gac-bep-pic-7.jpg"
                                            data-src="images/thit-trau-gac-bep-pic-7.jpg"
                                            class="attachment-original size-original lazy-load-active"
                                            alt="Trâu gác bếp" ids="29718,29719,29720,29721,29722,29723,29724,29725"
                                            style="none" lightbox_image_size="original" type="slider"
                                            col_spacing="xsmall" columns__sm="2" slider_nav_color="light"
                                            auto_slide="4000" image_height="230px" image_size="original"
                                            visibility="show-for-small"
                                            srcset="images/thit-trau-gac-bep-pic-7.jpg 560w, images/thit-trau-gac-bep-pic-7-300x300.jpg 300w, images/thit-trau-gac-bep-pic-7-150x150.jpg 150w"
                                            data-srcset="images/thit-trau-gac-bep-pic-7.jpg 560w, images/thit-trau-gac-bep-pic-7-300x300.jpg 300w, images/thit-trau-gac-bep-pic-7-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 100%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-8.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-8.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-8.jpg 560w, images/thit-trau-gac-bep-pic-8-300x300.jpg 300w, images/thit-trau-gac-bep-pic-8-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 150%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-1.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-1.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-1.jpg 560w, images/thit-trau-gac-bep-pic-1-300x300.jpg 300w, images/thit-trau-gac-bep-pic-1-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 200%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-2.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-2.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-2.jpg 560w, images/thit-trau-gac-bep-pic-2-300x300.jpg 300w, images/thit-trau-gac-bep-pic-2-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 250%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-3.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-3.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-3.jpg 560w, images/thit-trau-gac-bep-pic-3-300x300.jpg 300w, images/thit-trau-gac-bep-pic-3-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 300%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-4.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-4.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-4.jpg 560w, images/thit-trau-gac-bep-pic-4-300x300.jpg 300w, images/thit-trau-gac-bep-pic-4-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-col col is-selected" aria-selected="true"
                        style="position: absolute; left: 350%;">
                        <div class="col-inner">
                            <a class="image-lightbox lightbox-gallery" href="images/thit-trau-gac-bep-pic-5.jpg"
                                title="Trâu gác bếp">
                                <div class="box has-hover gallery-box box-none">
                                    <div class="box-image image-cover" style="padding-top:230px;">
                                        <img width="560" height="560"
                                            src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20560%20230%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                            data-src="images/thit-trau-gac-bep-pic-5.jpg"
                                            class="lazy-load attachment-original size-original" alt="Trâu gác bếp"
                                            ids="29718,29719,29720,29721,29722,29723,29724,29725" style="none"
                                            lightbox_image_size="original" type="slider" col_spacing="xsmall"
                                            columns__sm="2" slider_nav_color="light" auto_slide="4000"
                                            image_height="230px" image_size="original" visibility="show-for-small"
                                            srcset=""
                                            data-srcset="images/thit-trau-gac-bep-pic-5.jpg 560w, images/thit-trau-gac-bep-pic-5-300x300.jpg 300w, images/thit-trau-gac-bep-pic-5-150x150.jpg 150w"
                                            sizes="(max-width: 560px) 100vw, 560px">
                                    </div>
                                    <div class="box-text text-left">
                                        <p>Trâu gác bếp</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <button class="flickity-button flickity-prev-next-button previous" type="button" aria-label="Previous"
                disabled="">
                <svg class="flickity-button-icon" viewBox="0 0 100 100">
                    <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path>
                </svg>
            </button>
            <button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next" disabled="">
                <svg class="flickity-button-icon" viewBox="0 0 100 100">
                    <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"
                        transform="translate(100, 100) rotate(180) "></path>
                </svg>
            </button>
        </div>
    </div>
    <style>
        #section_660260776 {
            padding-top: 50px;
            padding-bottom: 50px;
        }

    </style>
</section>
