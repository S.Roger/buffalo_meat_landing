<section class="section hide-for-small" id="section_555581108">
    <div class="bg section-bg fill bg-fill  bg-loaded">





    </div>

    <div class="section-content relative">


        <div class="row" id="row-1016367526">


            <div id="col-906872822" class="col medium-12 small-12 large-12">
                <div class="col-inner">



                    <div class="container section-title-container" style="margin-bottom:0px;">
                        <h2 class="section-title section-title-center"><b></b><span class="section-title-main"
                                style="font-size:110%;">THỊT TRÂU GÁC BẾP TÂY BẮC</span><b></b></h2>
                    </div>

                    <p style="text-align: center;">Trâu gác bếp Tây Bắc chính hiệu từ thịt trâu sạch, không chất bảo
                        quản – Đặc sản gia truyền của người Thái Đen</p>
                    <div class="row row-small" id="row-676985914">


                        <div id="col-367043431" class="col medium-4 small-12 large-4">
                            <div class="col-inner text-center">



                                <div class="pricing-table-wrapper">
                                    <div class="pricing-table ux_price_table text-center box-shadow-5-hover"
                                        style="border-radius:30px;">
                                        <div class="pricing-table-header">
                                            <div class="title uppercase strong">Trâu gác bếp 0,5 kg</div>
                                            <div class="price is-xxlarge">415.000đ</div>
                                            <div class="description is-small">
                                                Thịt trâu khô gác bếp dạng miếng </div>
                                        </div>
                                        <div class="pricing-table-items items">


                                            <div class="bullet-item" title=""><span class="text">Quy cách: Đóng gói
                                                    0,5kg</span></div>

                                            <div class="bullet-item" title=""><span class="text">Trong túi hút chân
                                                    không</span></div>

                                            <div class="bullet-item" title=""><span class="text">Hạn dùng: 6
                                                    tháng</span></div>

                                            <div class="bullet-item tooltip has-hover tooltipstered"><span
                                                    class="text">Bảo quản ngăn đá</span><span
                                                    class="tag-label bullet-more-info circle">?</span></div>

                                            <a href="#dat_mua" target="_self" class="button primary expand"
                                                style="border-radius:99px;">
                                                <span>Đặt mua</span>
                                                <i class="icon-angle-right"></i></a>



                                        </div>
                                    </div>
                                </div>


                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1448114147">
                                    <a class="" href="https://shopee.vn/product/33997924/6006075172/" target="_blank"
                                        rel="noopener noreferrer">
                                        <div class="img-inner dark" style="margin:12px 0px -10px 0px;">
                                            <img width="160" height="56" src="images/shopee.png"
                                                data-src="images/shopee.png"
                                                class="attachment-large size-large lazy-load-active" alt="shopee">
                                        </div>
                                    </a>
                                    <style>
                                        #image_1448114147 {
                                            width: 32%;
                                        }

                                    </style>
                                </div>



                            </div>
                        </div>



                        <div id="col-1438547274" class="col medium-4 small-12 large-4">
                            <div class="col-inner text-center">



                                <div class="pricing-table-wrapper">
                                    <div class="pricing-table ux_price_table text-center box-shadow-5 box-shadow-4-hover"
                                        style="border-radius:30px;">
                                        <div class="pricing-table-header">
                                            <div class="title uppercase strong">Trâu gác bếp 1 kg</div>
                                            <div class="price is-xxlarge">820.000đ</div>
                                            <div class="description is-small">
                                                Trâu khô gác bếp dạng miếng, tảng </div>
                                        </div>
                                        <div class="pricing-table-items items">


                                            <div class="bullet-item" title=""><span class="text">Quy cách: Đóng gói
                                                    1kg</span></div>

                                            <div class="bullet-item" title=""><span class="text">Trong túi hút chân
                                                    không</span></div>

                                            <div class="bullet-item" title=""><span class="text">Hạn dùng: 6
                                                    tháng</span></div>

                                            <div class="bullet-item tooltip has-hover tooltipstered"><span
                                                    class="text">Bảo quản ngăn đá</span><span
                                                    class="tag-label bullet-more-info circle">?</span></div>

                                            <a href="#dat_mua" target="_self" class="button primary expand"
                                                style="border-radius:99px;">
                                                <span>Đặt mua</span>
                                                <i class="icon-angle-right"></i></a>



                                        </div>
                                    </div>
                                </div>


                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1925080030">
                                    <a class="" href="https://shopee.vn/product/33997924/6006075172/" target="_blank"
                                        rel="noopener noreferrer">
                                        <div class="img-inner dark" style="margin:12px 0px -10px 0px;">
                                            <img width="160" height="56" src="images/shopee.png"
                                                data-src="images/shopee.png"
                                                class="attachment-large size-large lazy-load-active" alt="shopee">
                                        </div>
                                    </a>
                                    <style>
                                        #image_1925080030 {
                                            width: 32%;
                                        }

                                    </style>
                                </div>



                            </div>
                        </div>



                        <div id="col-1559225109" class="col medium-4 small-12 large-4">
                            <div class="col-inner text-center">



                                <div class="pricing-table-wrapper">
                                    <div class="pricing-table ux_price_table text-center box-shadow-5-hover"
                                        style="border-radius:30px;">
                                        <div class="pricing-table-header">
                                            <div class="title uppercase strong">TRÂU GÁC BẾP GIÁ SỈ</div>
                                            <div class="price is-xxlarge">Liên hệ</div>
                                            <div class="description is-small">
                                                Đặt bán sỉ buôn từ 5kg trở lên </div>
                                        </div>
                                        <div class="pricing-table-items items">


                                            <div class="bullet-item" title=""><span class="text">Quy cách: Gói
                                                    0,5-1kg</span></div>

                                            <div class="bullet-item" title=""><span class="text">Trong túi hút chân
                                                    không</span></div>

                                            <div class="bullet-item" title=""><span class="text">Hạn dùng: 6
                                                    tháng</span></div>

                                            <div class="bullet-item tooltip has-hover tooltipstered"><span
                                                    class="text">Đặt hàng trước 3 ngày</span><span
                                                    class="tag-label bullet-more-info circle">?</span></div>

                                            <a href="#dat_mua" target="_self" class="button primary expand"
                                                style="border-radius:99px;">
                                                <span>Đặt mua</span>
                                                <i class="icon-angle-right"></i></a>



                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>



                        <div id="col-271627601" class="col hide-for-small medium-6 small-12 large-6">
                            <div class="col-inner">



                                <p><em><span style="color: #ff6600;">*</span><strong>Giao hàng toàn quốc</strong> với số
                                        lượng bất kỳ. Vận chuyển khu vực Hà Nội: <strong>60 phút</strong>, giao hàng khu
                                        vực TP HCM trong vòng 24h. Thanh toán COD.</em></p>

                            </div>

                            <style>
                                #col-271627601>.col-inner {
                                    padding: 12px 0px 0px 0px;
                                    margin: 0px 0px -10px 0px;
                                }

                            </style>
                        </div>



                        <div id="col-1493166515" class="col hide-for-small medium-6 small-12 large-6">
                            <div class="col-inner">



                                <p><span style="color: #ed1c24;">**</span> <strong>Trâu gác bếp</strong> của chúng tôi
                                    đảm bảo chất lượng, độ khô vừa phải và chắc chắn <strong>ngon hơn</strong> thịt trâu
                                    khô nơi khác – Có độ ướt cao.</p>

                            </div>

                            <style>
                                #col-1493166515>.col-inner {
                                    padding: 12px 0px 0px 0px;
                                    margin: 0px 0px -10px 0px;
                                }

                            </style>
                        </div>




                        <style>
                            #row-676985914>.col>.col-inner {
                                padding: 20px 0px 0px 0px;
                            }

                        </style>
                    </div>

                </div>

                <style>
                    #col-906872822>.col-inner {
                        margin: 0px 0px -30px 0px;
                    }

                </style>
            </div>



            <div id="col-542681429" class="col small-12 large-12">
                <div class="col-inner" style="background-color:rgb(255, 251, 171);">



                    <p><span style="text-decoration: underline;"><span style="color: #ff6600;"><strong>Lưu
                                    ý</strong></span></span>: Nguyên liệu <strong>trâu tươi</strong> đã có giá trên
                        230.000đ/kg, tối thiểu <strong>3,2kg trâu tươi</strong> mới ra thành phẩm <strong>1kg trâu
                            khô</strong> nên nhiều nơi bán <a title="Thị trâu gác bếp"
                            href="https://vi.wikipedia.org/wiki/Th%E1%BB%8Bt_g%C3%A1c_b%E1%BA%BFp" target="_blank"
                            rel="nofollow noopener noreferrer">Thịt gác bếp</a> có giá <strong>rẻ hơn
                            800.000đ/kg</strong> thì <strong>không phải thịt trâu khô</strong>. Hoặc pha trộn Thịt lợn,
                        hoặc Thịt trâu Ấn Độ giá rẻ để giảm giá.</p>

                </div>

                <style>
                    #col-542681429>.col-inner {
                        padding: 20px 30px 10px 30px;
                        margin: 0px 0px -40px 0px;
                    }

                </style>
            </div>



        </div>

    </div>


    <style>
        #section_555581108 {
            padding-top: 50px;
            padding-bottom: 50px;
            background-color: rgb(243, 244, 250);
        }

    </style>
</section>
