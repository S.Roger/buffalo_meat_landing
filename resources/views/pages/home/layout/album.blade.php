<section class="section hide-for-small" id="section_1231552344">
    <div class="bg section-bg fill bg-fill  bg-loaded">


    </div>

    <div class="section-content relative">


        <div class="row" id="row-1486698799">


            <div id="col-173302189" class="col small-12 large-12">
                <div class="col-inner">


                    <div class="row large-columns-3 medium-columns-3 small-columns-2 row-small">
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-hn-8.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-hn-8.jpg"
                                                data-src="images/trau-gac-bep-hn-8.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-hn-8.jpg 786w, imagestrau-gac-bep-hn-8-300x200.jpg 300w, images/trau-gac-bep-hn-8.jpg 768w"
                                                data-srcset="images/trau-gac-bep-hn-8.jpg 786w, imagestrau-gac-bep-hn-8-300x200.jpg 300w, images/trau-gac-bep-hn-8.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-29.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-29.jpg"
                                                data-src="images/trau-gac-bep-29.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-29.jpg 786w, images/trau-gac-bep-29-300x200.jpg 300w, images/trau-gac-bep-29.jpg 768w"
                                                data-srcset="images/trau-gac-bep-29.jpg 786w, images/trau-gac-bep-29-300x200.jpg 300w, images/trau-gac-bep-29.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-hn-2.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-hn-2.jpg"
                                                data-src="images/trau-gac-bep-hn-2.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Thịt trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-hn-2.jpg 786w, imagestrau-gac-bep-hn-2-300x200.jpg 300w, images/trau-gac-bep-hn-2.jpg 768w"
                                                data-srcset="images/trau-gac-bep-hn-2.jpg 786w, imagestrau-gac-bep-hn-2-300x200.jpg 300w, images/trau-gac-bep-hn-2.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-hn-1.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-hn-1.jpg"
                                                data-src="images/trau-gac-bep-hn-1.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Thịt trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-hn-1.jpg 786w, imagestrau-gac-bep-hn-1-300x200.jpg 300w, images/trau-gac-bep-hn-1.jpg 768w"
                                                data-srcset="images/trau-gac-bep-hn-1.jpg 786w, imagestrau-gac-bep-hn-1-300x200.jpg 300w, images/trau-gac-bep-hn-1.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-hn-3.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-hn-3.jpg"
                                                data-src="images/trau-gac-bep-hn-3.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-hn-3.jpg 786w, imagestrau-gac-bep-hn-3-300x200.jpg 300w, images/trau-gac-bep-hn-3.jpg 768w"
                                                data-srcset="images/trau-gac-bep-hn-3.jpg 786w, imagestrau-gac-bep-hn-3-300x200.jpg 300w, images/trau-gac-bep-hn-3.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="gallery-col col" data-animate="bounceInUp" data-animated="true">
                            <div class="col-inner">
                                <a class="image-lightbox lightbox-gallery" href="images/trau-gac-bep-19.jpg"
                                    title="Trâu gác bếp">
                                    <div class="box has-hover gallery-box box-none">
                                        <div class="box-image image-zoom">
                                            <img width="786" height="524" src="images/trau-gac-bep-19.jpg"
                                                data-src="images/trau-gac-bep-19.jpg"
                                                class="attachment-original size-original lazy-load-active"
                                                alt="Trâu gác bếp" ids="28708,29814,28702,28701,28703,29813"
                                                style="none" col_spacing="small" columns="3" columns__md="3"
                                                animate="bounceInUp" image_size="original" image_hover="zoom"
                                                srcset="images/trau-gac-bep-19.jpg 786w, images/trau-gac-bep-19-300x200.jpg 300w, images/trau-gac-bep-19.jpg 768w"
                                                data-srcset="images/trau-gac-bep-19.jpg 786w, images/trau-gac-bep-19-300x200.jpg 300w, images/trau-gac-bep-19.jpg 768w"
                                                sizes="(max-width: 786px) 100vw, 786px">
                                        </div>
                                        <div class="box-text text-left">
                                            <p>Trâu gác bếp</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>


                </div>

                <style>
                    #col-173302189>.col-inner {
                        margin: 20px 0px -30px 0px;
                    }

                </style>
            </div>


        </div>
    </div>
    <style>
        #section_1231552344 {
            padding-top: 30px;
            padding-bottom: 30px;
        }

    </style>
</section>
