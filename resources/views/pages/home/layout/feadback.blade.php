<section class="section" id="section_1074515658">
    <div class="bg section-bg fill bg-fill  bg-loaded">





    </div>

    <div class="section-content relative">


        <div class="container section-title-container" style="margin-top:20px;">
            <h2 class="section-title section-title-center"><b></b><span class="section-title-main"
                    style="font-size:120%;">Cảm nhận khách hàng</span><b></b></h2>
        </div>

        <div class="row row-small hide-for-medium" id="row-2106110069">


            <div id="col-1158684907" class="col medium-6 small-12 large-6">
                <div class="col-inner">



                    <div class="icon-box testimonial-box icon-box-left text-left">
                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                            <img width="150" height="150" src="images/dao-tuan-thanh-150x150.jpg"
                                data-src="images/dao-tuan-thanh-150x150.jpg"
                                class="attachment-thumbnail size-thumbnail lazy-load-active"
                                alt="Đào Tuấn Thành - Thịt trâu gác bếp"
                                srcset="images/dao-tuan-thanh-150x150.jpg 150w, images/dao-tuan-thanh-100x100.jpg 100w, images/dao-tuan-thanh.jpg 300w"
                                data-srcset="images/dao-tuan-thanh-150x150.jpg 150w, images/dao-tuan-thanh-100x100.jpg 100w, images/dao-tuan-thanh.jpg 300w"
                                sizes="(max-width: 150px) 100vw, 150px">
                        </div>
                        <div class="icon-box-text p-last-0">
                            <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span>
                            </div>
                            <div
                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                <h4>Trâu gác bếp thơm ngon, vị ngọt tự nhiên</h4>
                                <p>Rất bất ngờ về mùi vị của <a title="Trâu gác bếp"
                                        href="https://www.youtube.com/watch?v=CMysjs1Gwxo" target="_blank"
                                        rel="noopener noreferrer">trâu gác bếp Sơn La</a> mua từ đây nó đặc biệt làm sao
                                    ấy! Từ lần đầu tiên được ăn và tôi vẫn nhớ mãi hương vị ngon ngọt đậm đà, khó quên.
                                </p>

                            </div>
                            <div class="testimonial-meta pt-half">
                                <strong class="testimonial-name test_name">Tuấn Thành</strong>
                                <span class="testimonial-name-divider"> / </span> <span
                                    class="testimonial-company test_company">Hapro Group</span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>



            <div id="col-2029178724" class="col medium-6 small-12 large-6">
                <div class="col-inner">



                    <div class="icon-box testimonial-box icon-box-left text-left">
                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                            <img width="150" height="150" src="images/do-minh-duc-150x150.jpg"
                                data-src="images/do-minh-duc-150x150.jpg"
                                class="attachment-thumbnail size-thumbnail lazy-load-active"
                                alt="Đỗ Minh Đức - Thịt trâu gác bếp"
                                srcset="images/do-minh-duc-150x150.jpg 150w, images/do-minh-duc-100x100.jpg 100w, images/do-minh-duc-300x300.jpg 300w, images/do-minh-duc.jpg 400w"
                                data-srcset="images/do-minh-duc-150x150.jpg 150w, images/do-minh-duc-100x100.jpg 100w, images/do-minh-duc-300x300.jpg 300w, images/do-minh-duc.jpg 400w"
                                sizes="(max-width: 150px) 100vw, 150px">
                        </div>
                        <div class="icon-box-text p-last-0">
                            <div class="star-rating"><span style="width:75%"><strong class="rating"></strong></span>
                            </div>
                            <div
                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                <h4>Nhậu lai rai với bia rượu vừa mồm lắm</h4>
                                <p>Quê tôi món này đầy nhưng ăn không ngon bằng mua ở đây. Trước được bạn cho, nhậu riết
                                    thấy quen mồm, rất ngon và bác thấy nó vừa vừa, dai dai lại ngọt ngọt.</p>

                            </div>
                            <div class="testimonial-meta pt-half">
                                <strong class="testimonial-name test_name">Đỗ Minh Đức</strong>
                                <span class="testimonial-name-divider"> / </span> <span
                                    class="testimonial-company test_company">Thái Bình</span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>



            <div id="col-1322412839" class="col medium-6 small-12 large-6">
                <div class="col-inner">



                    <div class="icon-box testimonial-box icon-box-left text-left">
                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                            <img width="150" height="150" src="images/Tran-Thanh-Huyen-150x150.jpg"
                                data-src="images/Tran-Thanh-Huyen-150x150.jpg"
                                class="attachment-thumbnail size-thumbnail lazy-load-active"
                                alt="Trần Thanh Huyền - Thịt trâu gác bếp"
                                srcset="images/Tran-Thanh-Huyen-150x150.jpg 150w, images/Tran-Thanh-Huyen-100x100.jpg 100w, images/Tran-Thanh-Huyen-300x300.jpg 300w, images/Tran-Thanh-Huyen.jpg 400w"
                                data-srcset="images/Tran-Thanh-Huyen-150x150.jpg 150w, images/Tran-Thanh-Huyen-100x100.jpg 100w, images/Tran-Thanh-Huyen-300x300.jpg 300w, images/Tran-Thanh-Huyen.jpg 400w"
                                sizes="(max-width: 150px) 100vw, 150px">
                        </div>
                        <div class="icon-box-text p-last-0">
                            <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span>
                            </div>
                            <div
                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                <h4>Sản phẩm trâu khô cho nhà hàng rất hợp</h4>
                                <p>Công việc bên mình rất cần những nhà cung ứng sản phẩm chất lượng tốt, phù hợp nhu
                                    cầu thực khách. Mình rất hài lòng chất lượng trâu gác bếp của các bạn.</p>

                            </div>
                            <div class="testimonial-meta pt-half">
                                <strong class="testimonial-name test_name">Ngô Thanh Huyền</strong>
                                <span class="testimonial-name-divider"> / </span> <span
                                    class="testimonial-company test_company">Vietnam Food</span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>



            <div id="col-1389691112" class="col medium-6 small-12 large-6">
                <div class="col-inner">



                    <div class="icon-box testimonial-box icon-box-left text-left">
                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                            <img width="150" height="150" src="images/tran-minh-ha-150x150.jpg"
                                data-src="images/tran-minh-ha-150x150.jpg"
                                class="attachment-thumbnail size-thumbnail lazy-load-active"
                                alt="Trần Minh Hà - Thịt trâu gác bếp"
                                srcset="images/tran-minh-ha-150x150.jpg 150w, images/tran-minh-ha-100x100.jpg 100w, images/tran-minh-ha-300x300.jpg 300w, images/tran-minh-ha.jpg 400w"
                                data-srcset="images/tran-minh-ha-150x150.jpg 150w, images/tran-minh-ha-100x100.jpg 100w, images/tran-minh-ha-300x300.jpg 300w, images/tran-minh-ha.jpg 400w"
                                sizes="(max-width: 150px) 100vw, 150px">
                        </div>
                        <div class="icon-box-text p-last-0">
                            <div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span>
                            </div>
                            <div
                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                <h4>Thịt trâu gác bếp làm quà biếu rất hay</h4>
                                <p>Lần đầu mua trâu khô gác bếp về làm quà cho gia đình, rất bất ngờ là ai cũng khen
                                    ngon. Ban đầu nhìn đen đen không hiểu ăn thế nào, sau nghiện luôn.</p>

                            </div>
                            <div class="testimonial-meta pt-half">
                                <strong class="testimonial-name test_name">Trần Minh Hà</strong>
                                <span class="testimonial-name-divider"> / </span> <span
                                    class="testimonial-company test_company">IOV Việt Nam</span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>




            <style>
                #row-2106110069>.col>.col-inner {
                    padding: 14px 16px 18px 16px;
                    background-color: rgb(243, 244, 250);
                }

            </style>
        </div>
        <div class="slider-wrapper relative show-for-medium" id="slider-1051476255"
            style="background-color:rgb(243, 244, 250);">
            <div class="slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal is-draggable flickity-enabled slider-lazy-load-active"
                data-flickity-options="{
            &quot;cellAlign&quot;: &quot;center&quot;,
            &quot;imagesLoaded&quot;: true,
            &quot;lazyLoad&quot;: 1,
            &quot;freeScroll&quot;: false,
            &quot;wrapAround&quot;: true,
            &quot;autoPlay&quot;: 4000,
            &quot;pauseAutoPlayOnHover&quot; : true,
            &quot;prevNextButtons&quot;: true,
            &quot;contain&quot; : true,
            &quot;adaptiveHeight&quot; : true,
            &quot;dragThreshold&quot; : 10,
            &quot;percentPosition&quot;: true,
            &quot;pageDots&quot;: true,
            &quot;rightToLeft&quot;: false,
            &quot;draggable&quot;: true,
            &quot;selectedAttraction&quot;: 0.1,
            &quot;parallax&quot; : 0,
            &quot;friction&quot;: 0.6        }" tabindex="0">







                <div class="flickity-viewport" style="height: 0px; touch-action: pan-y;">
                    <div class="flickity-slider" style="left: 0px; transform: translateX(-100%);">
                        <div class="row align-center" id="row-709006785" aria-selected="false"
                            style="position: absolute; left: 0%;">


                            <div id="col-453209785" class="col medium-8 small-12 large-12">
                                <div class="col-inner">



                                    <div class="icon-box testimonial-box icon-box-center text-center">
                                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                                            <img width="150" height="150"
                                                src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20150%20150%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                                data-src="images/dao-tuan-thanh-150x150.jpg"
                                                class="lazy-load attachment-thumbnail size-thumbnail"
                                                alt="Đào Tuấn Thành - Thịt trâu gác bếp" srcset=""
                                                data-srcset="images/dao-tuan-thanh-150x150.jpg 150w, images/dao-tuan-thanh-100x100.jpg 100w, images/dao-tuan-thanh.jpg 300w"
                                                sizes="(max-width: 150px) 100vw, 150px">
                                        </div>
                                        <div class="icon-box-text p-last-0">
                                            <div class="star-rating"><span style="width:100%"><strong
                                                        class="rating"></strong></span></div>
                                            <div
                                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                                <h4>Thịt gác bếp thơm ngon, vị ngọt tự nhiên</h4>
                                                <p>Rất bất ngờ về mùi vị của Trâu gác bếp mua từ đây nó đặc biệt làm sao
                                                    ấy! Từ lần đầu tiên được ăn và tôi vẫn nhớ mãi hương vị ngon ngọt
                                                    đậm đà, khó quên.</p>

                                            </div>
                                            <div class="testimonial-meta pt-half">
                                                <strong class="testimonial-name test_name">Tuấn Thành</strong>
                                                <span class="testimonial-name-divider"> / </span> <span
                                                    class="testimonial-company test_company">Hapro Group</span>
                                            </div>
                                        </div>
                                    </div>



                                </div>

                                <style>
                                    #col-453209785>.col-inner {
                                        padding: 16px 0px 0 0px;
                                    }

                                </style>
                            </div>



                        </div>
                        <div class="row align-center is-selected" id="row-2056290627" aria-selected="true"
                            style="position: absolute; left: 100%;">


                            <div id="col-1161233811" class="col medium-8 small-12 large-12">
                                <div class="col-inner">



                                    <div class="icon-box testimonial-box icon-box-center text-center">
                                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                                            <img width="150" height="150"
                                                src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20150%20150%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                                data-src="images/do-minh-duc-150x150.jpg"
                                                class="lazy-load attachment-thumbnail size-thumbnail"
                                                alt="Đỗ Minh Đức - Thịt trâu gác bếp" srcset=""
                                                data-srcset="images/do-minh-duc-150x150.jpg 150w, images/do-minh-duc-100x100.jpg 100w, images/do-minh-duc-300x300.jpg 300w, images/do-minh-duc.jpg 400w"
                                                sizes="(max-width: 150px) 100vw, 150px">
                                        </div>
                                        <div class="icon-box-text p-last-0">
                                            <div class="star-rating"><span style="width:75%"><strong
                                                        class="rating"></strong></span></div>
                                            <div
                                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                                <h4>Nhậu lai rai với bia rượu vừa mồm lắm</h4>
                                                <p>Quê tôi món này đầy nhưng ăn không ngon bằng mua ở đây. Trước được
                                                    bạn cho, nhậu riết thấy quen mồm, rất ngon và bác thấy nó vừa vừa,
                                                    dai dai lại ngọt ngọt.</p>

                                            </div>
                                            <div class="testimonial-meta pt-half">
                                                <strong class="testimonial-name test_name">Đỗ Minh Đức</strong>
                                                <span class="testimonial-name-divider"> / </span> <span
                                                    class="testimonial-company test_company">Thái Bình</span>
                                            </div>
                                        </div>
                                    </div>



                                </div>

                                <style>
                                    #col-1161233811>.col-inner {
                                        padding: 16px 0px 0px 0px;
                                    }

                                </style>
                            </div>



                        </div>
                        <div class="row align-center" id="row-238663039" aria-selected="false"
                            style="position: absolute; left: 200%;">


                            <div id="col-545521043" class="col medium-8 small-12 large-12">
                                <div class="col-inner">



                                    <div class="icon-box testimonial-box icon-box-center text-center">
                                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                                            <img width="150" height="150"
                                                src="data:image/svg+xml,%3Csvg%20viewBox%3D%220%200%20150%20150%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3C%2Fsvg%3E"
                                                data-src="images/Tran-Thanh-Huyen-150x150.jpg"
                                                class="lazy-load attachment-thumbnail size-thumbnail"
                                                alt="Trần Thanh Huyền - Thịt trâu gác bếp" srcset=""
                                                data-srcset="images/Tran-Thanh-Huyen-150x150.jpg 150w, images/Tran-Thanh-Huyen-100x100.jpg 100w, images/Tran-Thanh-Huyen-300x300.jpg 300w, images/Tran-Thanh-Huyen.jpg 400w"
                                                sizes="(max-width: 150px) 100vw, 150px">
                                        </div>
                                        <div class="icon-box-text p-last-0">
                                            <div class="star-rating"><span style="width:100%"><strong
                                                        class="rating"></strong></span></div>
                                            <div
                                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                                <h4>Sản phẩm trâu khô cho nhà hàng rất hợp</h4>
                                                <p>Công việc bên&nbsp;mình rất cần những nhà cung ứng sản phẩm chất
                                                    lượng tốt, phù hợp nhu cầu&nbsp;thực khách. Mình rất hài lòng chất
                                                    lượng và dịch vụ sản phẩm nơi đây</p>

                                            </div>
                                            <div class="testimonial-meta pt-half">
                                                <strong class="testimonial-name test_name">Ngô Thanh Huyền</strong>
                                                <span class="testimonial-name-divider"> / </span> <span
                                                    class="testimonial-company test_company">Vietnam Food</span>
                                            </div>
                                        </div>
                                    </div>



                                </div>

                                <style>
                                    #col-545521043>.col-inner {
                                        padding: 16px 0px 0px 0px;
                                    }

                                </style>
                            </div>



                        </div>
                        <div class="row align-center" id="row-1725742940" aria-selected="false"
                            style="position: absolute; left: 300%;">


                            <div id="col-767680128" class="col medium-8 small-12 large-12">
                                <div class="col-inner">



                                    <div class="icon-box testimonial-box icon-box-center text-center">
                                        <div class="icon-box-img testimonial-image circle" style="width: 120px">
                                            <img width="150" height="150" src="images/tran-minh-ha-150x150.jpg"
                                                data-src="images/tran-minh-ha-150x150.jpg"
                                                class="attachment-thumbnail size-thumbnail lazy-load-active"
                                                alt="Trần Minh Hà - Thịt trâu gác bếp"
                                                srcset="images/tran-minh-ha-150x150.jpg 150w, images/tran-minh-ha-100x100.jpg 100w, images/tran-minh-ha-300x300.jpg 300w, images/tran-minh-ha.jpg 400w"
                                                data-srcset="images/tran-minh-ha-150x150.jpg 150w, images/tran-minh-ha-100x100.jpg 100w, images/tran-minh-ha-300x300.jpg 300w, images/tran-minh-ha.jpg 400w"
                                                sizes="(max-width: 150px) 100vw, 150px">
                                        </div>
                                        <div class="icon-box-text p-last-0">
                                            <div class="star-rating"><span style="width:100%"><strong
                                                        class="rating"></strong></span></div>
                                            <div
                                                class="testimonial-text line-height-small italic test_text first-reset last-reset is-italic">


                                                <h4>Thịt gác bếp làm quà biếu rất hay</h4>
                                                <p>Tôi đã mua sản phẩm trâu gác bếp này về làm quà cho gia đình, rất bất
                                                    ngờ là ai cũng khen quá ngon. Ban đầu nhìn đen đen cũng không hiểu
                                                    ăn thế nào, sau nghiện 🙂</p>

                                            </div>
                                            <div class="testimonial-meta pt-half">
                                                <strong class="testimonial-name test_name">Trần Minh Hà</strong>
                                                <span class="testimonial-name-divider"> / </span> <span
                                                    class="testimonial-company test_company">IOV Việt Nam</span>
                                            </div>
                                        </div>
                                    </div>



                                </div>

                                <style>
                                    #col-767680128>.col-inner {
                                        padding: 16px 0px 0px 0px;
                                    }

                                </style>
                            </div>



                        </div>
                    </div>
                </div><button class="flickity-button flickity-prev-next-button previous" type="button"
                    aria-label="Previous"><svg class="flickity-button-icon" viewBox="0 0 100 100">
                        <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path>
                    </svg></button><button class="flickity-button flickity-prev-next-button next" type="button"
                    aria-label="Next"><svg class="flickity-button-icon" viewBox="0 0 100 100">
                        <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"
                            transform="translate(100, 100) rotate(180) "></path>
                    </svg></button>
                <ol class="flickity-page-dots">
                    <li class="dot" aria-label="Page dot 1"></li>
                    <li class="dot is-selected" aria-label="Page dot 2" aria-current="step"></li>
                    <li class="dot" aria-label="Page dot 3"></li>
                    <li class="dot" aria-label="Page dot 4"></li>
                </ol>
            </div>

            <div class="loading-spin dark large centered" style="display: none;"></div>

        </div>



    </div>


    <style>
        #section_1074515658 {
            padding-top: 30px;
            padding-bottom: 30px;
        }

    </style>
</section>
