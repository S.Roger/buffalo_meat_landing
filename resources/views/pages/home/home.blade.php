@extends('index')
@section('title', 'Trâu gác bếp hà nội tp hcm giá rẻ, trâu khô gác bếp loại 1')
@section('content')
    <div id="content" role="main">
        {{--  Clear fix  --}}
        <div id="gap-1473426169" class="gap-element clearfix show-for-small" style="display:block; height:auto;">
            <style>
                #gap-1473426169 {
                    padding-top: 70px;
                }

                @media (min-width: 550px) {
                    #gap-1473426169 {
                        padding-top: 30px;
                    }
                }
            </style>
        </div>
        {{--  Banner  --}}
        @include('pages.home.layout.banner')
        {{--  Banner small   --}}
        @include('pages.home.layout.banner_small')
        {{--Introduce--}}
        @include('pages.home.layout.introduce')
        {{--    section đặc sản--}}
        @include('pages.home.layout.specialties')
        {{--    List image--}}
        @include('pages.home.layout.album')
        {{--       List product & Buy--}}
        @include('pages.home.layout.product')
        {{--        more--}}
        @include('pages.home.layout.more')
        {{--        feadback--}}
        @include('pages.home.layout.feadback')
        {{--contact--}}
        @include('pages.home.layout.contact')
        {{--        faq--}}
        @include('pages.home.layout.faq')
    </div>
@endsection
